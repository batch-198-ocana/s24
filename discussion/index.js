console.log("Hello, World!");

// exponent operator
    let fivePowerOf3 = Math.pow(5,3);
    console.log(fivePowerOf3);
    // Math.pow(baseNum, exponent) allows us to get the exponent of a number

    let fivePowerof2 = 5 ** 2;
    console.log(fivePowerof2);
    // baseNum ** exponent

    let squareRootof4 = 4 ** .5;
    console.log(squareRootof4);

// concatenate strings
    let string1 = "Javascript";
    let string2 = "not";
    let string3 = "is";
    let string4 = "Typescript";
    let string5 = "Java";
    let string6 = "Zuitt";
    let string7 = "Coding";

        // let sentence1 = string1 +" " +string3 +" " +string2 +" " + string5;
        // let sentence2 = string4 +" " +string3 +" " +string1;
        // console.log(sentence1);
        // console.log(sentence2);

    // template literals
    let sentence1 = `${string1} ${string3} ${string2} ${string5}`;
    let sentence2 = `${string1} ${string3} ${string4}`;
    console.log(sentence1);
    console.log(sentence2);

    let sentence3 = `${string6} ${string7} Bootcamp`;
    console.log(sentence3);

    let sentence4 = `The sum of 15 and 25 is ${15+25}`;
    console.log(sentence4);

    let person = {
        name: "Michael",
        position: "developer",
        income: 50000,
        expenses: 60000
    }
    console.log(`${person.name} is a ${person.position}`);
    console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}`);

// desturcting arrays and objects : allows us to save array items and objects properties into new variables
    let array1 = ["Curry","Lillard","Paul","Irving"];
    let [player1,player2,player3,player4] = array1;
    console.log(player1,player2,player3,player4);

    let array2 = ["Jokic","Embiid","Howard","Anthony-Towns"];
    // get and save all items into a new variables except for "Howard"
    let [center1,center2,,center4] = array2;
    console.log(center4);

    // destructing objects
    let pokemon1 = {
        name: "Bulbasaur",
        type: "Grass",
        level: 10,
        moves: ["Razor Leaf","Tackle","Leech seed"]
    }

    let {level,type,name,moves,personality} = pokemon1;

    console.log(level);
    console.log(type);
    console.log(name);
    console.log(moves);
    console.log(personality);

    let pokemon2 = {
        name: "Charmander",
        type: "Fire",
        level: 1,
        moves: ["Ember","Scratch","Leech seed"]
    }
    // {propertyname: newvariable}
    const {name: name2} =pokemon2;
    console.log(name2);

// Arrow function : alternative way of writing a function
    // tradition function
    function displayMsg(){
        console.log('Hello, World');
    }
    displayMsg();

    // arrow function
    const hello = () => {
        console.log(`Hello from arrow`);
    }
    hello();

    // arrow function with parameters
    const greet = (friend) => {
        console.log(`Hi ${friend.name}`);
    }
    greet(person);

    //arrow vs traditional function
        // function add(num1, num2){
        //     let result = num1 + num2;
        //     return result;
        // }

        // let sum = add(5,10);
        // console.log(sum);

    //implicit return : return values w/o the return keyword
    // it will only work without {}
    // if an arrow function has a code block, need to use a return keyword
    let sub = (num1,num2) => num1 - num2;
    let difference = sub(10,5);
    console.log(difference);

    let add = (num1,num2) => num1 + num2;

    let sum = add(50,70);
    console.log(sum);

    // traditional function vs arrow function as methods

    let character1 = {
        name: "Cloud Strife",
        occupation: "SOLDIER",
        greet: function(){
            // this keyword refers to the current object (character1{})
            console.log(this);
            console.log(`Hi I'm ${this.name}`);
        },
        introduceJob: () => {
            // in an arrow function this keyword will NOT refer to the current object. Instead will refer to the global window object.
            console.log(this);
        }
    }

    character1.greet();
    character1.introduceJob();

// class based object blueprint
    // classes are templates of objects
    // we can create objects out of the use of classes
    
    // constuctor function
        // function Pokemon(name,type,level){
        //     this.name = name;
        //     this.type = type;
        //     this.level = level;
        // }
        // let pokemon3 = new Pokemon("Eevee","Normal",32);
        // console.log(pokemon3);

    // ES6
    class Car{
        constructor(brand,name,year){
            this.brand = brand;
            this.name = name;
            this.year = year;
        }
    }
    let car1 = new Car("Toyota","Vios","2002");
    console.log(car1);

// mini activity
    class Pokemon{
        constructor(name,type,level){
            this.name = name;
            this.type = type;
            this.level = level;
        }
    }

    let pokemon3 = new Pokemon("Eevee","Normal",32);
    let pokemon4 = new Pokemon("Chimchar","Fire",43);

    console.log(pokemon3);
    console.log(pokemon4);